## Instalation
From this folder ```python setup.py install```

## Usage
```python

from point2 import Point2

# initializing as (0, 0)
p = Point2()
print p # (rect(0.0, 0.0), polar(0.0, 0.0))

# initializing with x and y
p = Point2(10, 15)
print p # (rect(10, 15), polar(18.0277563773, 0.982793723247))

# initializing from a tuple (only for rectangular coordinates)
p = Point2( (10, 15) )
print p # (rect(10, 15), polar(18.0277563773, 0.982793723247))
# or even maybe
t = (20, 20)
p = Point2(t)
print p # (rect(20, 20), polar(28.2842712475, 0.785398163397))

# initializing from another point (copy)
p2 = Point2(p)
print p2 # (rect(10, 15), polar(18.0277563773, 0.982793723247))

# the coordinates are rectangular(x, y) and polar(r, a)
# from here you can set any of the coordinates and the other ones will be recalculated

p.x = 0
print p # (rect(0, 15), polar(15.0, 1.57079632679))
p.a = 0
print p # (rect(15.0, 0.0), polar(15.0, 0))

# getting coordinates
p = Point2(10, 10)
print p.x # 10
print p.y # 10
print p.r # 14.1421356237
print p.a # 0.785398163397

# getting rectangular coordinates as a tuple
print p.rect() # (10, 10)

# getting polar coordinates as a tuple
print p.polar() # (14.142135623730951, 0.7853981633974483)



# operations between two points
# the operations return a new point, they don't change the points used on the operations
p1 = Point2(10, 0)
p2 = Point2(0, 10)
print p1 + p2 # (rect(10, 10), polar(14.1421356237, 0.785398163397))
print p1 - p2 # (rect(10, -10), polar(14.1421356237, -0.785398163397))

# unless you do this
p1 += p2
print p1 # (rect(10, 10), polar(14.1421356237, 0.785398163397))

# you can multiply by a scalar
p = Point2(1, 0)
p *= 10
print p # (rect(10.0, 0.0), polar(10.0, 0.0))
```

## Tricks

### Length of a vector

This one should be pretty clear. `point.r` will tell you the length of the vector. If you want to be very clear you can use `len(point)` **but remember: len only returns integers**

### Distance between points

If you think about it, subtracting two points (P1 and P2) will give you a third point (P3). The length of this point (P3) is the distance between you two first points (P1 and P2)
```python
p1 = Point2(0, 0)
p2 = Point2(3, 4)
(p1 - p2).r # 5.0
# If you want to be very clear you can do it like this:
p1.distance(p2) # 5.0
# But if you check the code, the distance function just returns (p1 - p2).r
```

### Angle between points

If you, again, subtract two points, the angle of the result point is the angle between the points.
```python
p1 = Point2(0, 0)
p2 = Point2(4, 4)
(p2 - p1).a # 0.7853981633974483 which is pi/4 which is 45 degrees which is correct
```
Be careful with the order, though. `(p1 - p2).a` is not the same as `(p2 - p1).a`. In this example `(p1 - p2).a` = -2.356194490192345 = -3pi/4 = -135 degrees = 225 degrees. That is the opposite angle and it's ok if that's what you wanted but if you were expecting 45 degrees that would cause you problems so be careful.

### Scaling vectors
You can do this by multiplying the point by a number or multiplying its radius by a number.

```python
p = Point2(1, 0)
p *= 3   # (rect(3.0, 0.0), polar(3.0, 0.0))
p.r *= 3 # (rect(9.0, 0.0), polar(9.0, 0.0))
```

### Unitary vector
The unitary vector is the same version of that point with length equals to 1. Well, that's easy to do.
```python
p = Point2(22, 13.634) # (rect(22, 13.634), polar(25.8821551653, 0.554798702935))
p.r = 1 # (rect(0.85000649519, 0.526772207064), polar(1, 0.554798702935))
# you're welcome
```