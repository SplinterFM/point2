
## Problems that you might face in your program that I can help you with
### I have a list of tuples representing rectangular coordinates but I would like to use your Point2. How do I convert them?
Well, I would use list comprehension.
```python
tuples = [ (1,1), (2,2), (3,3) ]
points2 = [Point2(t[0], t[1]) for t in tuples]
print points2 # [(rect(1, 1), polar(1.41421356237, 0.785398163397)), (rect(2, 2), polar(2.82842712475, 0.785398163397)), (rect(3, 3), polar(4.24264068712, 0.785398163397))]
```
### Now I need to convert them back. OMG!
Even easier.
```python
tuples = [(p.x, p.y) for p in points2]
print tuples # [(1, 1), (2, 2), (3, 3)]
```



